/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {},
    screens: {
      sm: '768px',
      md: '900px',
      lg: '1280px',
      xl: '1536px',
    },
    colors: {
      'dark-blue': '#2a3c46',
      orange: '#f29e60',
      white: '#ffffff',
    },
    maxWidth: {
      '8xl': '90rem',
    },
  },
  plugins: [require('@headlessui/tailwindcss')],
}
