FROM node:16-alpine AS builder
ARG CI
WORKDIR /app
COPY . /app 
RUN npm install
ENV ASTRO_WITH_DENO=1 
RUN npm run build 


FROM denoland/deno:alpine
WORKDIR /app
USER deno
COPY --from=builder /app/public .
CMD ["run", "--allow-all", "./server/entry.mjs"]


