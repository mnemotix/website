import { defineConfig } from 'astro/config'
import mdx from '@astrojs/mdx'
import sitemap from '@astrojs/sitemap'
import tailwind from '@astrojs/tailwind'
import react from '@astrojs/react'
import compressor from 'astro-compressor'
import compress from 'astro-compress'
import env from 'env-var'
import deno from '@astrojs/deno'

let extraConfig = {}

if (env.get('ASTRO_WITH_DENO').asBool() === true) {
  extraConfig = {
    output: 'server',
    adapter: deno(),
  }
}

// https://astro.build/config
export default defineConfig({
  site: 'https://www.mnemotix.com',
  integrations: [
    mdx(),
    sitemap(),
    tailwind(),
    react(),
    compress({
      img: false,
      svg: false,
    }),
    compressor(),
  ],
  output: 'static',
  // GitLab Pages requires exposed files to be located in a folder called "public".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: './public',

  // The folder name Astro uses for static files (`public`) is already reserved
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: './static',
  ...extraConfig,
})
