import { Menu } from '@headlessui/react';

export default function MobileNav({children} = {}){
  return (
    <Menu className="relative" as="div">
      <Menu.Button aria-label="Bouton de menu">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
        <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
      </svg>
      </Menu.Button>
      <Menu.Items className="absolute right-0 bg-white backdrop-blur supports-backdrop-blur:bg-white/95 p-4 shadow-md">
        {children}
      </Menu.Items>
    </Menu>
  )

}