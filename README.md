# [Mnemotix.com](https://www.mnemotix.com) website

This site is built with [Astro.build](https://astro.build) and [TailwindCSS](https://tailwindcss.com/)

## Structure

Inside of your Astro project, you'll see the following folders and files:

```
├── static/
├── src/
│   ├── components/
│   ├── layouts/
│   └── pages/
├── astro.config.mjs
├── README.md
├── package.json
└── tsconfig.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `static/` directory.

## Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                           |
| :--------------------- | :----------------------------------------------- |
| `npm install`          | Installs dependencies                            |
| `npm run dev`          | Starts local dev server at `localhost:3000`      |
| `npm run build`        | Build your production site to `./dist/`          |
| `npm run preview`      | Preview your build locally, before deploying     |
| `npm run astro ...`    | Run CLI commands like `astro add`, `astro check` |
| `npm run astro --help` | Get help using the Astro CLI                     |

## Deployment

The deployment process is handled by GitlabCI pipeline each time a Git tag is created and pushed.

The production version of the website is served by a DenoJS server, choose for it's simplicity, efficiency and compression capabilities.

See [.gitlab-ci.yml](./.gitlab-ci.yml) and [Dockerfile](Dockerfile) files for more information.
